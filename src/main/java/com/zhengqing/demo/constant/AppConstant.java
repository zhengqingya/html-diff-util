package com.zhengqing.demo.constant;

import java.util.regex.Pattern;

/**
 * <p>
 * 全局常用变量
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2019/10/12 14:47
 */
public interface AppConstant {

    // ===============================================================================
    // ============================ ↓↓↓↓↓↓ 文件系列 ↓↓↓↓↓↓ ============================
    // ===============================================================================

    /**
     * 系统分隔符
     */
    String SYSTEM_SEPARATOR = "/";

    /**
     * 文件组和文件路径分割符
     */
    String GROUP_PATH_DELIMITER = "//";

    /**
     * 获取项目根目录
     */
    String PROJECT_ROOT_DIRECTORY = System.getProperty("user.dir").replaceAll("\\\\", SYSTEM_SEPARATOR);

    // ============================================================================================================
    // ============================ ↓↓↓↓↓↓ 文本差异对比系列(新增、删除、修改、对调、不变、移动) ↓↓↓↓↓↓ ======================
    // ============================================================================================================

    /**
     * 正则获取对比差异内容
     */
    Pattern DIFF_PATTERN_CONTENT = Pattern.compile("<(ins|del|) class='.*?'>(([\\s\\S])*?)</(ins|del|)>");

    /**
     * 颜色标识html代码
     */
    String TEXT_DIFF_HTML_CODE_TEXT_INSERT =
        "<span data-version=\"%s\" data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"增加\" class=\"text-add\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_DELETE =
        "<span data-version=\"%s\" data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"删除\" class=\"text-delete\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_UPDATE =
        "<span data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"text-update\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_EXCHANGE_BEFORE =
        "<span data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"text-exchange-before\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_EXCHANGE_AFTER =
        "<span data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"text-exchange-after\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_EQUAL = "<span data-operator-id=\"%s\" data-operator-name=\"%s\" >%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_MOVE_BEFORE =
        "<span data-move-id=\"%s\" data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"移动\" class=\"text-move-before\">%s</span>";
    String TEXT_DIFF_HTML_CODE_TEXT_MOVE_AFTER =
        "<span data-move-id=\"%s\" data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"移动\" class=\"text-move-after\">%s</span>";

    /**
     * 图片crud
     */
    String TEXT_DIFF_HTML_CODE_IMG_INSERT =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"增加\" class=\"img-add\" src=\"%s\" width=\"%s\" height=\"%s\">";
    String TEXT_DIFF_HTML_CODE_IMG_DELETE =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"删除\" class=\"img-delete\" src=\"%s\" width=\"%s\" height=\"%s\">";
    String TEXT_DIFF_HTML_CODE_IMG_UPDATE =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"img-update\" src=\"%s\" width=\"%s\" height=\"%s\">";
    String TEXT_DIFF_HTML_CODE_IMG_EXCHANGE_BEFORE =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"img-exchange-before\" src=\"%s\" width=\"%s\" height=\"%s\">";
    String TEXT_DIFF_HTML_CODE_IMG_EXCHANGE_AFTER =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" data-title=\"原来：%s\" class=\"img-exchange-after\" src=\"%s\" width=\"%s\" height=\"%s\">";
    String TEXT_DIFF_HTML_CODE_IMG_EQUAL =
        "<img data-operator-id=\"%s\" data-operator-name=\"%s\" src=\"%s\" width=\"%s\" height=\"%s\">";

    // 图片/表格 替换标识处理（需配置少见标识）
    String TEXT_DIFF_HTML_CODE_IMG_TAG_OLD = "☀";
    String TEXT_DIFF_HTML_CODE_IMG_TAG_NEW = "♬";
    String TEXT_DIFF_HTML_CODE_TABLE_TAG = "☂";

    // 正则获取`<div>`中内容
    Pattern TEXT_PATTERN_CONTENT_DIV = Pattern.compile("<div(([\\s\\S])*?)</div>");
    // 正则获取`data-title`中内容
    Pattern TEXT_PATTERN_CONTENT_DATA_TITLE = Pattern.compile("data-title=\"(([\\s\\S])*?)\"");
    // 正则获取`<p>`中内容
    Pattern TEXT_PATTERN_CONTENT_P = Pattern.compile("<p(([\\s\\S])*?)</p>");
    // 正则作替换除`\n\r`以外内容加标签使用
    Pattern TEXT_PATTERN_REPLACE_CONTENT_LINE_FEED = Pattern.compile(".+");
    // 正则取`\n\r`开始|结尾，取中间的内容，但不排除中间内容的`\n\r`
    Pattern TEXT_PATTERN_CENTER_CONTENT_LINE_FEED = Pattern.compile("[^\\n\\r].+[^\\n\\r]");

    // 前后端换行符
    String TEXT_LINE_FEED_CODE_JAVA = "\n\r";
    String TEXT_LINE_FEED_CODE_VUE = "<br>";

    // 正则取img标签中的img值、src值、宽高
    Pattern TEXT_PATTERN_IMG = Pattern.compile("<(img|IMG)(.*?)(>|></img>|/>)");
    Pattern TEXT_PATTERN_IMG_SRC = Pattern.compile("(src|SRC)=(\"|\')(.*?)(\"|\')");
    Pattern TEXT_PATTERN_IMG_WIDTH = Pattern.compile("(width|WIDTH)=(\"|\')(.*?)(\"|\')");
    Pattern TEXT_PATTERN_IMG_HEIGHT = Pattern.compile("(height|HEIGHT)=(\"|\')(.*?)(\"|\')");

    // 正则取table标签中的数据
    Pattern TEXT_PATTERN_TABLE = Pattern.compile("<table(([\\s\\S])*?)</table>");

    // 去除html字符中所有的标签（img标签 | br 除外） -> 将匹配到的数据全部替换为空，最后剩下的就是所需要的数据
    String TEXT_PATTERN_TAKE_OUT_HTML_KEEP_IMG_AND_BR = "<(?!(img)|(\\/?br\\/?.+?>))[^<>]*>";
    String TEXT_PATTERN_TAKE_OUT_HTML_KEEP_BR = "<(?!\\/?br\\/?.+?>)[^<>]*>";

    /**
     * 对比差异样式css
     */
    String DIFF_CSS = "<link href=\"diff.css\" rel=\"stylesheet\" type=\"text/css\">";

}
