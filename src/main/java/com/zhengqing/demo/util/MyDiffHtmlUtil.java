package com.zhengqing.demo.util;

import java.util.List;

import com.zhengqing.demo.model.bo.DiffBO;
import com.zhengqing.demo.model.bo.DiffDataBO;
import com.zhengqing.demo.model.bo.DiffTextInfoHandleBO;

/**
 * <p>
 * 对比文本差异工具类
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/12/9 9:48
 */
public class MyDiffHtmlUtil {

    /**
     * 获取文本差异信息
     *
     * @param params:
     *            参数信息
     * @return: 差异文本信息数据
     * @author : zhengqing
     * @date : 2020/12/9 9:48
     */
    public static DiffBO getDiffData(DiffTextInfoHandleBO params) {
        MyDiffHtmlHandleUtil myDiffHtmlHandleUtil = new MyDiffHtmlHandleUtil();
        return myDiffHtmlHandleUtil.diff(params);
    }

    /**
     * 获取文本差异信息
     *
     * @param htmlOld:
     *            旧文本内容
     * @param htmlNew:
     *            新文本内容
     * @return 差异文本信息数据
     * @author zhengqingya
     * @date 2021/3/12 9:26
     */
    public static DiffBO getDiffData(String htmlOld, String htmlNew) {
        MyDiffHtmlHandleUtil myDiffHtmlHandleUtil = new MyDiffHtmlHandleUtil();
        return myDiffHtmlHandleUtil
            .diff(DiffTextInfoHandleBO.builder().htmlOld(htmlOld).htmlNew(htmlNew).ifAllDiffType(true).build());
    }

    /**
     * 获取文本差异信息
     *
     * @param htmlOld:
     *            旧文本内容
     * @param htmlNew:
     *            新文本内容
     * @param checkedDiffTypeList:
     *            选择的对比操作类型数据
     * @return 差异文本信息数据
     * @author zhengqingya
     * @date 2021/3/12 9:27
     */
    public static DiffBO getDiffData(String htmlOld, String htmlNew, List<Integer> checkedDiffTypeList) {
        MyDiffHtmlHandleUtil myDiffHtmlHandleUtil = new MyDiffHtmlHandleUtil();
        return myDiffHtmlHandleUtil.diff(DiffTextInfoHandleBO.builder().htmlOld(htmlOld).htmlNew(htmlNew)
            .checkedDiffTypeList(checkedDiffTypeList).build());
    }

    /**
     * 正则表达式替换匹配内容并为其加标签 - 针对原生数据（新增/删除）
     *
     * @param diffList:
     *            对比数据
     * @return: 拿到对比后差异文本信息
     * @author : zhengqing
     * @date : 2021/1/19 15:40
     */
    public static String addTagForBasicText(List<DiffDataBO> diffList) {
        MyDiffHtmlHandleUtil myDiffHtmlHandleUtil = new MyDiffHtmlHandleUtil();
        return myDiffHtmlHandleUtil.addTagForBasicText(diffList);
    }

}
