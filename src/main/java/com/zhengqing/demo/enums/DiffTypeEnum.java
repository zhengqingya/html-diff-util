package com.zhengqing.demo.enums;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;
import com.zhengqing.demo.exception.MyException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;

/**
 * <p>
 * 对比类型枚举
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/10/14 13:47
 */
@Getter
@AllArgsConstructor
public enum DiffTypeEnum {

    不变(0, "不变"), 增加(1, "增加"), 删除(2, "删除"), 修改(3, "修改"), 对调(4, "对调"), 移动(5, "移动");

    private final Integer type;
    private final String desc;

    private static final List<DiffTypeEnum> LIST = Lists.newArrayList();

    static {
        LIST.addAll(Arrays.asList(DiffTypeEnum.values()));
    }

    /**
     * 根据指定的对比分类查找相应枚举类
     *
     * @param type:
     *            对比分类
     * @return: 枚举信息
     * @author : zhengqing
     * @date : 2020/10/14 13:48
     */
    @SneakyThrows(Exception.class)
    public static DiffTypeEnum getEnum(Integer type) {
        for (DiffTypeEnum itemEnum : LIST) {
            if (itemEnum.getType().equals(type)) {
                return itemEnum;
            }
        }
        throw new MyException("未找到指定的对比类型分类！");
    }

    /**
     * 根据指定的对比标识查找相应枚举类
     *
     * @param tag:
     *            对比标识
     * @return: 枚举信息
     * @author : zhengqing
     * @date : 2020/12/4 16:55
     */
    @SneakyThrows(Exception.class)
    public static DiffTypeEnum getEnumByTag(String tag) {
        switch (tag) {
            case "ins":
                return 增加;
            case "del":
                return 删除;
            default:
                break;
        }
        throw new MyException("未找到指定的对比类型分类！");
    }

}
