package com.zhengqing.demo.model.bo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比文本处理参数信息
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/10/29 14:13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比文本处理参数信息")
public class DiffTextInfoHandleBO {

    @ApiModelProperty(value = "旧文本内容（富文本）")
    private String htmlOld;

    @ApiModelProperty(value = "新文本内容（富文本）")
    private String htmlNew;

    @ApiModelProperty(value = "是否支持全部对比类型 true:是  false：否 （当为否的时候才从`checkedDiffTypeList`参数中获取值）")
    private Boolean ifAllDiffType;

    @ApiModelProperty(value = "选择的对比操作类型数据(参考枚举类`DiffTypeEnum` 0：不变 1：增加 2：删除 3：修改 4：对调 5：移动)")
    private List<Integer> checkedDiffTypeList;

}
