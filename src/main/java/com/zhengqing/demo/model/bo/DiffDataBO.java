package com.zhengqing.demo.model.bo;

import com.zhengqing.demo.enums.DiffTypeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比文本信息
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/12/4 16:29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比文本信息")
public class DiffDataBO {

    @ApiModelProperty(value = "操作类型")
    private DiffTypeEnum typeEnum;

    @ApiModelProperty(value = "文本")
    private String text;

    @ApiModelProperty(value = "版本")
    private Integer version;

    @ApiModelProperty(value = "描述")
    private String des;

}
