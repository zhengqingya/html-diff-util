package com.zhengqing.demo.model.bo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比文本统计信息
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/10/22 11:46
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比文本统计信息")
public class DiffTextStatisticsInfoBO {

    @ApiModelProperty(value = "操作人id")
    private Integer operatorId;

    @ApiModelProperty(value = "操作人名称")
    private String operatorName;

    @ApiModelProperty(value = "新增文本处数量")
    private Integer addNum;

    @ApiModelProperty(value = "删除文本处数量")
    private Integer deleteNum;

    @ApiModelProperty(value = "修改文本处数量")
    private Integer updateNum;

    @ApiModelProperty(value = "对调文本处数量")
    private Integer exchangeNum;

    @ApiModelProperty(value = "移动文本处数量")
    private Integer moveNum;

    @ApiModelProperty(value = "新增文本差异内容数据")
    private List<DiffTextInfoItem> textInfoAddList;

    @ApiModelProperty(value = "删除文本差异内容数据")
    private List<DiffTextInfoItem> textInfoDeleteList;

    @ApiModelProperty(value = "修改文本差异内容数据")
    private List<DiffTextInfoItem> textInfoUpdateList;

    @ApiModelProperty(value = "对调文本差异内容数据")
    private List<DiffTextInfoItem> textInfoExchangeList;

    @ApiModelProperty(value = "对调文本差异内容数据")
    private List<DiffTextInfoItem> textInfoMoveList;

    @ApiModelProperty(value = "是否操作过数据")
    private Boolean ifOperated;

    @Data
    @ApiModel("对比文本信息详情")
    public static class DiffTextInfoItem {
        @ApiModelProperty(value = "操作文本数据")
        private String text;

        @ApiModelProperty(value = "在修改情况下的原来文本 - 即之前的删除文本(只在修改情况下，该值使用，其它情况没任何作用)")
        private String textBefore;

        @ApiModelProperty(value = "操作类型值 - 冗余字段")
        private Integer type;

        @ApiModelProperty(value = "操作类型名 - 冗余字段")
        private String typeName;
    }

}
