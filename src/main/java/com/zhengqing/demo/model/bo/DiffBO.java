package com.zhengqing.demo.model.bo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比文本信息
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/11/9 14:46
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比文本信息")
public class DiffBO {

    @ApiModelProperty(value = "差异统计信息")
    private DiffTextStatisticsInfoBO diffTextStatisticsInfo;

    @ApiModelProperty(value = "差异信息体现富文本文本")
    private String diffContentHtml;

    @ApiModelProperty(value = "对比文本信息是否显示样式数据")
    private List<DiffShowBO> diffShowList;

}
