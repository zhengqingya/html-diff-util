package com.zhengqing.demo.model.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比条件类型
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/11/4 10:31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比条件类型")
public class DiffConditionBO {

    @ApiModelProperty(value = "是否支持插入")
    private Boolean hasInsert;

    @ApiModelProperty(value = "删除")
    private Boolean hasDelete;

    @ApiModelProperty(value = "修改")
    private Boolean hasUpdate;

    @ApiModelProperty(value = "对调")
    private Boolean hasExChange;

    @ApiModelProperty(value = "移动")
    private Boolean hasMove;

}
