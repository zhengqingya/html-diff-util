package com.zhengqing.demo.model.bo;

import com.zhengqing.demo.enums.DiffTypeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 对比文本信息
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/12/4 16:29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对比文本信息")
public class DiffShowBO {

    @ApiModelProperty(value = "表示数据位置（用于移动数据时判断匹配数据进行改变操作类型标识）")
    private Integer index;

    @ApiModelProperty(value = "是否显示样式")
    private Boolean ifShow;

    @ApiModelProperty(value = "对比内容")
    private DiffDataBO diff;

    @ApiModelProperty(value = "在修改情况下的原来文本 - 即之前的删除文本 (注：只在`修改`操作情况下，该值使用，其它情况没任何作用)")
    private String diffTextBefore;

    @ApiModelProperty(value = "对比文本")
    private String diffText;

    @ApiModelProperty(value = "是否移除当前元素  (注：只在`修改`操作情况下，该值使用，其它情况没任何作用)")
    private Boolean ifRemoveCurrentElement;

    @ApiModelProperty(value = "操作类型")
    private DiffTypeEnum diffTypeEnum;

}
