package com.zhengqing.demo.diff.text;

import java.util.LinkedList;

import org.junit.Test;

import com.zhengqing.demo.diff.text.Diff_match_patch.Diff;

/**
 * <p>
 * 小测试$
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/7/10$ 11:02$
 */
public class DiffTextSimpleTest {

    @Test
    public void testHtmlDiff() throws Exception {
        Diff_match_patch dmp = new Diff_match_patch();
        String text1 = "Hello,this is my test data.";
        String text2 = "hi,this is your test data?";

        LinkedList<Diff> diffList = dmp.diff_main(text1, text2);
        String diffTextResult = dmp.diff_prettyHtml(diffList);
        System.out.println("\n" + diffTextResult + "\n");
    }

}
