package com.zhengqing.demo.diff.html;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.assertj.core.util.Lists;
import org.junit.Test;

import com.zhengqing.demo.constant.AppConstant;
import com.zhengqing.demo.enums.DiffTypeEnum;
import com.zhengqing.demo.model.bo.DiffBO;
import com.zhengqing.demo.model.bo.DiffDataBO;
import com.zhengqing.demo.model.bo.DiffShowBO;
import com.zhengqing.demo.model.bo.DiffTextInfoHandleBO;
import com.zhengqing.demo.util.MyDiffHtmlUtil;
import com.zhengqing.demo.util.MyFileUtil;

/**
 * <p>
 * 小测试$
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/7/10$ 11:02$
 */
public class DiffHtmlMergeTest {

    @Test
    public void testHtmlDiffMerge() throws Exception {
        String htmlOld = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test-merge-init.html");
        String htmlNew1 = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test-merge-1.html");
        String htmlNew2 = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test-merge-2.html");
        String htmlNew3 = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test-merge-3.html");

        // 求对比数据
        DiffBO diffData1 = MyDiffHtmlUtil.getDiffData(DiffTextInfoHandleBO.builder()
            .checkedDiffTypeList(Lists.newArrayList(DiffTypeEnum.增加.getType(), DiffTypeEnum.删除.getType()))
            .ifAllDiffType(false).htmlOld(htmlOld).htmlNew(htmlNew1).build());
        DiffBO diffData2 = MyDiffHtmlUtil.getDiffData(DiffTextInfoHandleBO.builder()
            .checkedDiffTypeList(Lists.newArrayList(DiffTypeEnum.增加.getType(), DiffTypeEnum.删除.getType()))
            .ifAllDiffType(false).htmlOld(htmlOld).htmlNew(htmlNew2).build());
        DiffBO diffData3 = MyDiffHtmlUtil.getDiffData(DiffTextInfoHandleBO.builder()
            .checkedDiffTypeList(Lists.newArrayList(DiffTypeEnum.增加.getType(), DiffTypeEnum.删除.getType()))
            .ifAllDiffType(false).htmlOld(htmlOld).htmlNew(htmlNew3).build());

        // 求对比数据类型+文本
        List<DiffShowBO> diffShowList1 = diffData1.getDiffShowList();
        List<DiffShowBO> diffShowList2 = diffData2.getDiffShowList();
        List<DiffShowBO> diffShowList3 = diffData3.getDiffShowList();

        List<DiffDataBO> diffDataList1 = diffShowList1.stream().map(DiffShowBO::getDiff).collect(Collectors.toList());
        List<DiffDataBO> diffDataList2 = diffShowList2.stream().map(DiffShowBO::getDiff).collect(Collectors.toList());
        List<DiffDataBO> diffDataList3 = diffShowList3.stream().map(DiffShowBO::getDiff).collect(Collectors.toList());

        // 塞个版本信息
        diffDataList1.forEach(e -> e.setVersion(1));
        diffDataList2.forEach(e -> e.setVersion(2));
        diffDataList3.forEach(e -> e.setVersion(3));

        List<DiffDataBO> diffDataBOList = this.handleMergeData(diffDataList1, diffDataList2, diffDataList3);

        // 给对比文本加标签
        String diffContentHtml = MyDiffHtmlUtil.addTagForBasicText(diffDataBOList);

        // 写入内容到指定文件
        MyFileUtil.writeFileContent(diffContentHtml, AppConstant.PROJECT_ROOT_DIRECTORY + "/diff-merge-result.html");
    }

    /**
     * 处理对比差异版本信息
     *
     * @param elements:
     *            多个对比版本信息
     * @return: 对比合并结果
     * @author : zhengqing
     * @date : 2021/1/19 19:13
     */
    @SafeVarargs
    private final List<DiffDataBO> handleMergeData(List<DiffDataBO>... elements) {
        // 总list
        List<DiffDataBO> list = Lists.newArrayList();
        if (elements == null) {
            return list;
        }

        // 第1个list
        List<DiffDataBO> firstList = elements[0];
        int dataLength = elements.length;

        // 1、如果为1个版本则直接返回
        if (dataLength == 1) {
            return firstList;
        }

        list = firstList;

        // 2、处理2个版本以上的数据
        for (int i = 1; i < dataLength; i++) {
            // 依次处理相邻2list
            list = this.handleMergeDataPlus(list, elements[i], i == 1);

            // break; // FIXME 暂只处理2个版本数据
        }
        return list;
    }

    /**
     * 合并对比差异版本数据
     *
     * @param list1:
     *            数据1
     * @param list2:
     *            数据2
     * @param ifInit:
     *            是否首次对比
     * @return: 对比合并结果
     * @author : zhengqing
     * @date : 2021/1/19 19:12
     */
    private List<DiffDataBO> handleMergeDataPlus(List<DiffDataBO> list1, List<DiffDataBO> list2, boolean ifInit) {
        List<DiffDataBO> diffList = Lists.newArrayList();

        // list2 下标
        int index2 = 0;

        // 2、list1
        for (DiffDataBO item1 : list1) {
            // 类型、文本
            DiffTypeEnum diffTypeEnum1 = item1.getTypeEnum();
            String diffText1 = item1.getText();
            Integer version1 = item1.getVersion();

            // 2.1、如果是原始版本则直接加入 & 跳出循环
            if (!ifInit && version1 != 0) {
                diffList.add(DiffDataBO.builder().version(version1).typeEnum(diffTypeEnum1).text(diffText1).build());
                continue;
            }

            // 2.2、list2
            for (int j = index2; j < list2.size(); j++) {
                DiffDataBO item2 = list2.get(j);

                // 类型、文本
                DiffTypeEnum diffTypeEnum2 = item2.getTypeEnum();
                String diffText2 = item2.getText();
                Integer version2 = item2.getVersion();

                // 类型&文本值相等时 版本为0(即公共版本) ； 不等时为各自的版本
                if (diffTypeEnum1 == diffTypeEnum2 && diffText1.equals(diffText2)) {
                    diffList.add(DiffDataBO.builder().version(0).typeEnum(diffTypeEnum2).text(diffText2).build());
                    index2 = j + 1;
                    break;
                }

                diffList.add(DiffDataBO.builder().version(version2).typeEnum(diffTypeEnum2).text(diffText2).build());
                diffList.add(DiffDataBO.builder().version(version1).typeEnum(diffTypeEnum1).text(diffText1).build());

                index2 = j + 1;
                break;
            }
        }
        return diffList;
    }

    @Test
    public void test01() throws Exception {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int[] nums2 = {2, 5, 6};
        int m = 3;
        int n = 3;
        this.handleDataMerge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
    }

    private void handleDataMerge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1, j = n - 1, k = m + n - 1;
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j])
                nums1[k--] = nums1[i--];
            else
                nums1[k--] = nums2[j--];
        }
        while (j >= 0) {
            nums1[k--] = nums2[j--];
        }
    }

    @Test
    public void test02() throws Exception {
        int[] a = {10, 3, 5, 7, 9, 11, 13};
        int[] b = {2, 4, 6, 8, 10, 12};

        int[] c = new int[a.length + b.length];

        int i = 0;
        int j = 0;
        for (int k = 0; k < c.length; k++) {
            if (i < a.length && j < b.length) {
                if (a[i] <= b[j]) {
                    c[k] = a[i++];
                } else {
                    c[k] = b[j++];
                }
                // i < a.length && j >= b.length
            } else if (i < a.length) {
                c[k] = a[i++];
                // i >= a.length && j < b.length
            } else if (j < b.length) {
                c[k] = b[j++];
            } else {
                throw new RuntimeException("不可能情况");
            }
        }

        for (int k = 0; k < c.length; k++) {
            // System.out.println("c[" + k + "] = " + c[k]);
        }
        System.out.println(Arrays.toString(c));
    }

}
