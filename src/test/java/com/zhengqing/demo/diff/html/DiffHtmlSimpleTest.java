package com.zhengqing.demo.diff.html;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;

import com.zhengqing.demo.constant.AppConstant;
import com.zhengqing.demo.enums.DiffTypeEnum;
import com.zhengqing.demo.model.bo.DiffBO;
import com.zhengqing.demo.model.bo.DiffShowBO;
import com.zhengqing.demo.model.bo.DiffTextInfoHandleBO;
import com.zhengqing.demo.model.bo.DiffTextStatisticsInfoBO;
import com.zhengqing.demo.util.MyDiffHtmlUtil;
import com.zhengqing.demo.util.MyFileUtil;

/**
 * <p>
 * 小测试$
 * </p>
 *
 * @author : zhengqing
 * @description :
 * @date : 2020/7/10$ 11:02$
 */
public class DiffHtmlSimpleTest {

    @Test
    public void testDiff() throws Exception {
        String htmlOld = "22";
        String htmlNew = "11";

        DiffBO diffData = MyDiffHtmlUtil
            .getDiffData(DiffTextInfoHandleBO.builder().ifAllDiffType(true).htmlOld(htmlOld).htmlNew(htmlNew).build());

        String diffContentHtml = diffData.getDiffContentHtml();
        List<DiffShowBO> diffShowList = diffData.getDiffShowList();
        DiffTextStatisticsInfoBO diffTextStatisticsInfo = diffData.getDiffTextStatisticsInfo();

        System.out.println("\n" + diffContentHtml + "\n");
    }

    @Test
    public void testHtmlDiff() throws Exception {
        String htmlOld = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test1.html");
        String htmlNew = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test2.html");

        DiffBO diffData = MyDiffHtmlUtil.getDiffData(DiffTextInfoHandleBO.builder()
            .checkedDiffTypeList(Lists.newArrayList(DiffTypeEnum.增加.getType(), DiffTypeEnum.删除.getType(),
                DiffTypeEnum.修改.getType(), DiffTypeEnum.对调.getType(), DiffTypeEnum.移动.getType()))
            .htmlOld(htmlOld).htmlNew(htmlNew).build());

        String diffContentHtml = diffData.getDiffContentHtml();
        List<DiffShowBO> diffShowList = diffData.getDiffShowList();
        DiffTextStatisticsInfoBO diffTextStatisticsInfo = diffData.getDiffTextStatisticsInfo();

        MyFileUtil.writeFileContent(diffContentHtml, AppConstant.PROJECT_ROOT_DIRECTORY + "/diff.html");
    }

}
