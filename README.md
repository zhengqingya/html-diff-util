# html对比差异

1. `新增数据`
2. `删除数据`
3. `修改数据`
4. `对调数据`
5. `移动数据`

### 效果图

html1:
![html1](./images/html1.png)

html2:
![html2](./images/html2.png)

对比结果:
![html-diff](./images/html-diff.png)

### 使用

```java
public class DemoTest {

  @Test
  public void testHtmlDiff() throws Exception {
    String htmlOld = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test1.html");
    String htmlNew = MyFileUtil.readFileContent(AppConstant.PROJECT_ROOT_DIRECTORY + "/test2.html");

    DiffBO diffData = MyDiffHtmlUtil.getDiffData(DiffTextInfoHandleBO.builder()
        .checkedDiffTypeList(
            Lists.newArrayList(DiffTypeEnum.增加.getType(), DiffTypeEnum.删除.getType(),
                DiffTypeEnum.修改.getType(), DiffTypeEnum.对调.getType(), DiffTypeEnum.移动.getType()))
        .htmlOld(htmlOld).htmlNew(htmlNew).build());

    String diffContentHtml = diffData.getDiffContentHtml();
    List<DiffShowBO> diffShowList = diffData.getDiffShowList();
    DiffTextStatisticsInfoBO diffTextStatisticsInfo = diffData.getDiffTextStatisticsInfo();

    MyFileUtil.writeFileContent(diffContentHtml, AppConstant.PROJECT_ROOT_DIRECTORY + "/diff.html");
  }

}
```
